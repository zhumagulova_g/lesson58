import React, {useState} from 'react';
import Alert from "../Alert/Alert";

const ShowAlert = () => {
    const STYLE = {
        primary: 'primary',
        success: 'success',
        danger: 'danger',
        warning: 'warning'
    }
    const [closeBtn, setCloseBtn] = useState(false);
    const [closeModal, setCloseModal] = useState(false);

    const closing = () => {
      setCloseBtn(false);
    };
    const opening = () => {
        setCloseBtn(true);
        setCloseModal(true);
    }

    const closingModal = () => {
        setCloseModal(false);
    };

    return (
        <div>
            <button
            className="btn btn-success btn-show"
            onClick={opening}
        >
            SHOW ALERTS</button>

            <div>{(closeBtn) ?
                <div className="show-alerts">
                    <Alert
                    type={STYLE.warning}
                    dismiss={closing}
                    >This is a warning type alert</Alert>

                </div> : null
                }

                {(closeModal) ?
                <div className='show-alerts'>
                    <Alert
                        type={STYLE.success}
                        closingModal={closingModal}
                    >This is a success type alert</Alert>
                </div>: null
                }
            </div>
        </div>
    );
};

export default ShowAlert;