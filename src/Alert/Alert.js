import React from 'react';

const Alert = (props) => {
    return ((props.dismiss !== undefined) ?
        <div className={'alert show-alert alert-' + props.type} role="alert">
            {props.children}
            <button className="btn close-btn" onClick={props.dismiss}>X</button>
        </div> :
        <div className={'alert show-alert alert-' + props.type}
             role="alert"
             onClick={props.closingModal}>
            {props.children}
        </div>
    );
};

export default Alert;