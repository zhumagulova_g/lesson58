import React from 'react';
import Modal from '../Modal/Modal';

const ShowModal = (props) => {
    const CONFIGURATION = [{type: 'btn btn-primary', label: 'Continue'},
        {type: 'btn btn-danger', label: 'Close'}]

    const showAlert = () => {
        alert('My Alert!');
    };

    return (
        <div>
            <Modal
                show={props.show}
                closed={props.closed}
                showAlert={showAlert}
                title="Some kinda modal title"
                configuration={CONFIGURATION}
            >
                <p>This is modal content</p>
            </Modal>
            <button
                className="btn btn-info btn-show"
                onClick={props.showAlert}
            >
                SHOW MODAL</button>
        </div>
    );
};

export default ShowModal;