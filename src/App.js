import React, {useState} from 'react';
import './App.css';
import ShowModal from "./ShowModal/ShowModal";
import ShowAlert from "./ShowAlert/ShowAlert";

const App = () => {
    const [purchasing, setPurchasing] = useState(false);

    const purchaseHandler = () => {
        setPurchasing(true);
    };
    const purchaseCancelHandler =() => {
        setPurchasing(false);
    }
  return (
      <div>
          <ShowModal
            show={purchasing}
            showAlert={purchaseHandler}
            closed={purchaseCancelHandler}
          />
          <ShowAlert/>
      </div>
  );
};

export default App;
