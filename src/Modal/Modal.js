import React from 'react';
import Backdrop from "../Backdrop/Backdrop";
import './Modal.css';

const Modal = (props) => {
    return (
        <>
            <Backdrop show={props.show}
                      onClick={props.closed}
            />
            <div
                className="Modal"
                style={{
                    transform:  props.show ? 'translateY(0)' :'translateY(-100vh)',
                    opacity: props.show ? '1' : '0'
                }}
            >
                <div className="modal-dialog " >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">{props.title}</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close" onClick={props.closed}/>
                        </div>
                        <div className="modal-body">
                            <p>{props.children}</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className={props.configuration[0].type} onClick={props.showAlert}>{props.configuration[0].label} </button>
                            <button type="button" className={props.configuration[1].type}  data-bs-dismiss="modal" onClick={props.closed}>{props.configuration[1].label}</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Modal;